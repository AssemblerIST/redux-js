import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk'; // написание ассинхронных событий

import './index.css';
import App from './App';

import reducer from './reducers'
//import registerServiceWorker from './registerServiceWorker';
//соединение с devtools
//const composeEnhancers = composeWithDevTools(options);
//создание контейнера и передача функции для возвращения данных
// composeWithDevTools - автоматически добавляет devTools ко всему что в скобках
const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render(
  <Provider store = {store}>
    <App />
</Provider>,
  document.getElementById('root'));
//registerServiceWorker();
// import { createStore } from 'redux';
//
// function playList (state = [], action) {
//   if (action.type === 'ADD_TRACK'){
// // возвращает новый массив с добавленным элементом
//     return [
//       ...state,
//       action.payload
//     ];
//   }
//   return state;
// }
// //создание контейнера и передача функции для возвращения данных
// const store = createStore(playList);
//
// const trackInput = document.querySelectorAll('.trackInput')[0];
// const addTrackBtn = document.querySelectorAll('.addTrack')[0];
// const list = document.querySelectorAll('.list')[0];
// //подпись на изменение данных в контейнере
// store.subscribe(() => {
//   console.log('subscribe',store.getState());
//   list.innerHTML = '';
//   trackInput.value = '';
//   store.getState().forEach(track => {
//     const li = document.createElement('li');
//     li.textContent = track;
//     list.appendChild(li);
//   });
// })
// addTrackBtn.addEventListener('click',() => {
//   const trackName = trackInput.value;
//   console.log('trackName',trackName);
//   //добавление данных в контейнер
//   store.dispatch({type: 'ADD_TRACK', payload: trackName});
//
// });
