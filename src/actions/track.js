var mockApiData = [
  {
    id: '1',
    name: 'Enter Sandman'
  },
  {
    id: '2',
    name: 'Get hell'
  },
  {
    id: '3',
    name: 'Welcome to home'
  },
  {
    id: '4',
    name: 'Silent hill'
  },
  {
    id: '5',
    name: 'Hiruto'
  }
];

// возвращаем функцию с аргументом dispatch
export const getTracks = () => dispatch =>{
    setTimeout(() => {
      console.log('I got tracks');
      dispatch({type: 'FETCH_TRACKS_SUCCESS', payload: mockApiData })
    },2000)
}
