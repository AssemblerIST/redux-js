import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getTracks } from './actions/track'

class App extends Component {
  addTrack(){
    console.log('addTrack', this.trackInput.value);
    this.props.onAddTrack(this.trackInput.value);
    this.trackInput.value = '';
  }
  findTrack(){
    console.log('SearchTrack', this.searchInput.value);
    this.props.onFindTrack(this.searchInput.value);
  }
  render() {
    console.log(this.props.tracks);
    return (
      <div>
        <div>
          <input type="text" ref = {(input) => this.trackInput = input } />
          <button onClick = {this.addTrack.bind(this)}>Add track</button>
        </div>
        <div>
          <input type="text" ref = {(input) => this.searchInput = input } />
          <button onClick = {this.findTrack.bind(this)}>Search track</button>
        </div>
        <div>
          <button onClick ={this.props.onGetTracks}>Get tracks</button>
        </div>
        <ul>
          {this.props.tracks.map((track, index) =>
            <li key = {index}>{track.name}</li>
          )}
        </ul>
      </div>
    );
  }
}

export default connect(
// состояние store, необходимо для отслеживания изменений
  state => ({
    //все треки которые есть в store фильтруются по filterTrack
    tracks: state.tracks.filter(track => track.name.includes(state.filterTrack))
  }),
  dispatch => ({
// методы для разных событий
    onAddTrack: (name) => {
      const payload = {
        Id: Date.now().toString(),
        name
      };
      dispatch({type: 'ADD_TRACK', payload});
    },
    onFindTrack: (name) =>{
      dispatch({type: 'FIND_TRACK', payload: name})
    },
    // функция которая возвращает функцию которая диспатчит объекты по истечению времени
    onGetTracks:() =>{
      dispatch(getTracks());
    }
  })
)(App);
