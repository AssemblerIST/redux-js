const initialState = [];

export default function playList (state = initialState, action) {
// возвращает новый массив с добавленным элементом
if (action.type === 'ADD_TRACK'){
    return [
      ...state,
      action.payload
    ];
  } else if (action.type === 'FETCH_TRACKS_SUCCESS'){
    return action.payload;
  }
  return state;
}
