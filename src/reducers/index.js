import {combineReducers  } from 'redux';

import tracks from './track.js';
import playList from './playlist.js'
import filterTrack from './filtertrack.js'

export default combineReducers({
  tracks,
  playList,
  filterTrack
});
